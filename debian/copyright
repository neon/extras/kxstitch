Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kxstitch
Source: https://download.kde.org/stable/kxstitch
Upstream-Contact: kde-devel@kde.org

Files: *
Copyright: 2003-2015 Stephen Allewell <steve.allewell@gmail.com>
License: GPL-2+

Files: debian/*
Copyright: 2018, 2019 Scarlett Moore <sgmoore@kde.org>
  2015 Jeremy Whiting <jpwhiting@kde.org>
License: GPL-2+

Files: doc/*
Copyright: 2009-2013 Stephen Allewell <steve.allewell@gmail.com>
License: GFDL-1.2-without-invariant

Files: org.kde.kxstitch.appdata.xml
Copyright: 2009-2013 Stephen Allewell <steve.allewell@gmail.com>
License: GFDL-1.2

Files: po/*
Copyright: 2019 José Nuno Coelho Pires <zepires@gmail.com>
  2018 Tommi Nieminen <translator@legisign.org>
  2018 Simon Depiets <sdepiets@gmail.com>
  2016-2018 Karl Ove Hufthammer <karl@huftis.org>
  2016 Sofia Priego <spriego@darksylvania.net>
  2016 Jeff Huang <s8321414@gmail.com>
  2015-2019 Adrián Chaves Fernández <adrian@chaves.io>
  2015 enolp <enolp@softastur.org>
  2015 Weng Xuetian <wengxt@gmail.com>
  2015 Samir Ribic <samir.ribic@etf.unsa.ba>
  2015, 2019 Alexander Potashev <aspotashev@gmail.com>
  2015, 2018 Luiz Fernando Ranghetti <elchevive68@gmail.com>
  2014-2019 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
  2014-2019 Stefan Asserhäll <stefan.asserhall@bredband.net>
  2014-2019 Freek de Kruijf <freekdekruijf@kde.nl>
  2014-2019 Eloy Cuadra <ecuadra@eloihr.net>
  2014-2018 Vít Pelčák <vit@pelcak.org>
  2014-2018 Burkhard Lück <lueck@hube-lueck.de>
  2014-2016 Roman Paholik <wizzardsk@gmail.com>
  2014 zoe <chp321@gmail.com>
  2014 Sebastien Renard <renard@kde.org>
  2014 Ronald Stroethoff <stroet43@zonnet.nl>
  2014 Japanese KDE translation team <kde-jp@kde.org>
  2014 Frederik Schwarzer <schwarzer@kde.org>
  2014 Balázs Úr <urbalazs@gmail.com>
  2014, 2017 Ercole Carpanetto <ercole69@gmail.com>
  2014, 2016 Marek Laane <bald@smail.ee>
  2014, 2015 André Marcelo Alvarenga <alvarenga@kde.org>
  2011-2019 Steve Allewell <steve.allewell@gmail.com>
  2011, 2014 Joan Teixido Parramona <joan4@users.sourceforge.net>
License: GPL-2+

Files: po/ca/*
Copyright: 2015-2017 Ferrer,Antoni Bella <antonibella5@yahoo.com>
  2014-2019 Josep Ma. Ferrer <txemaq@gmail.com>
  2012, 2014 Joan Teixido Parramona <joan4@users.sourceforge.net>
License: LGPL-2.1 or LGPL-3 or KDEeV

Files: po/ca@valencia/*
Copyright: 2015-2017 Ferrer,Antoni Bella <antonibella5@yahoo.com>
  2014-2019 Josep Ma. Ferrer <txemaq@gmail.com>
  2012, 2014 Joan Teixido Parramona <joan4@users.sourceforge.net>
License: LGPL-2.1 or LGPL-3 or KDEeV

Files: po/uk/*
Copyright: 2014-2019 Yuri Chornoivan <yurchor@ukr.net>
License: LGPL-2.1 or LGPL-3 or KDEeV

Files: src/XKeyLock.cpp
  src/XKeyLock.h
Copyright: 2003 Aurelien Jarno
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: KDEeV
 Copyright (C) 2013-2016 This_file_is_part_of_KDE
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: GFDL-1.2
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GFDL-1.2"

License: GFDL-1.2-without-invariant
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
 Texts.  A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GFDL-1.2"
